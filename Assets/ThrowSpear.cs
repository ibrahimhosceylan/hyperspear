﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowSpear : MonoBehaviour {

    [SerializeField] private GameObject Spear;
    [SerializeField] private GameObject Floor;
    [SerializeField] private float degrees;
    private Rigidbody2D _rb;
    
    private BoxCollider2D _cd;
    private BoxCollider2D _cd2;
    


    void Awake()
    {   
        if (_rb == null)
        {

            _rb = Spear.AddComponent<Rigidbody2D>();
           
            _rb.gravityScale = 0;
            
        }

        if (_cd == null)
        {
            _cd = Spear.AddComponent<BoxCollider2D>();
        }

        if (_cd2 == null)
        {

            _cd2 = Floor.AddComponent<BoxCollider2D>();

        }       
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))

        {
            float radians = degrees * Mathf.Deg2Rad;
            Vector2 vec2 = new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
            _rb.AddForce(vec2 * 20, ForceMode2D.Impulse);
            _rb.gravityScale = 2;
        }
    
    }
    
}
