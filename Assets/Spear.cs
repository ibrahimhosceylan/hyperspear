﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spear : MonoBehaviour,IThrowable
{
    private Rigidbody2D _rb;

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public void Throw(Vector3 direction)
    {
        if (_rb == null)
        {
            _rb = gameObject.GetComponent<Rigidbody2D>();
        }
        
            _rb.isKinematic = false;
            _rb.AddForce(direction * 20, ForceMode2D.Impulse);     
    }
    void OnCollisionEnter2D(Collision2D bounce)
    {        
            if (bounce.gameObject.tag == "Hyper")
            {
            //_rb.AddForce(new Vector2(1, 1) * 10, ForceMode2D.Impulse);
            Destroy(this.gameObject);
            }
        }
    }

