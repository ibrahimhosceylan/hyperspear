﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    [SerializeField] private Spear _spear;
    [SerializeField] private GameObject _floor;
    [SerializeField] private float _degrees = 45.0f;
    private Vector2 _directionVector;
    private Rigidbody2D _rb;
    private BoxCollider2D _spearCol;
    private BoxCollider2D _floorCol;
    private float _radians;
    private bool _ready;
    private IThrowable _currentThrowable;
    

    void Awake()
    {
        _radians = _degrees * Mathf.Deg2Rad;
        _directionVector = new Vector2(Mathf.Cos(_radians), Mathf.Sin(_radians));
        _ready = false;
        if (_floorCol == null)
        {
            _floorCol = _floor.AddComponent<BoxCollider2D>();   
        }
    }
    void Update()
    {
        
        if (Input.GetMouseButtonDown(0) && _ready)
        {
            Throw();
        }
        if (Input.GetKeyDown(KeyCode.Space) && !_ready)
        {
            Spawner();
        }
    }

    private void Throw()
    {
        if (_currentThrowable == null)
        {
            
            return;
        }  
        _currentThrowable.Throw(_directionVector);
        _currentThrowable = null;
        _ready = false;
    }

    private void Spawner()
    {
       
        Spear newspear = Instantiate (_spear, transform.position, Quaternion.identity); 
        _rb = newspear.gameObject.AddComponent<Rigidbody2D>();
        _spearCol = newspear.gameObject.AddComponent<BoxCollider2D>();
        _rb.gravityScale = 2;
        _rb.isKinematic = true;
        _rb.collisionDetectionMode = CollisionDetectionMode2D.Continuous;   // 3. zıplamadan sonra düşüyordu. Bu şekilde çözüldü.  
        _currentThrowable = newspear;
        _ready = true;
    }

}
