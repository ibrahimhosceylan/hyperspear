﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{
    private Rigidbody2D _rb;
    private int _count = 2;

    private void FixedUpdate()
    {
        _rb = gameObject.GetComponent<Rigidbody2D>();
    }

    void OnCollisionEnter2D(Collision2D bounce)
    {
       
        
        if (_count != 0)
            {
            //Destroy(gameObject);
            if (bounce.gameObject.tag == "Hyper")
            {
                _rb.AddForce(new Vector2(1, 1) * 10, ForceMode2D.Impulse);
            }

            _count--;
            }

           
        }
    }

